import re
from typing import Iterator

from pydantic import BaseModel
from unidecode import unidecode


class EarthFixDat(BaseModel):
    latitude: float
    longitude: float
    name: str
    airport: str
    country: str
    designation: str

    def export_to_gpx(self):
        return f'\t<wpt lat="{self.latitude}" lon="{self.longitude}">\n' \
               f'\t\t<name>{self.airport} | {self.designation}</name>\n' \
               f'\t\t<sym>BlueDot</sym>\n' \
               f'\t\t<extensions>\n' \
               f'\t\t\t<identifier>{unidecode(self.name)}</identifier>\n' \
               f'\t\t</extensions>\n' \
               f'\t</wpt>\n'


def iterator_earth_fix_file() -> Iterator[EarthFixDat]:
    with open("earth_fix.dat", "r") as file:
        file_data = file.read()
        findings = re.finditer(
            r'\s?(?P<latitude>(?:-|)\d+.\d+)\s+(?P<longitude>(?:-|)\d+.\d+)\s+(?P<name>\w+)\s(?P<airport>\w+)\s'
            r'(?P<country>\w+)\s\d+\s(?P<designation>.*)\n',
            file_data)
        for point in findings:
            yield EarthFixDat(**point.groupdict())


def point_of_country(point: EarthFixDat, country_ident: str) -> bool:
    return point.country == country_ident


def points_to_gpx(country_ident: str):
    output = '<?xml version="1.0" encoding="UTF-8"?>\n' \
             '<gpx version="1.0">\n'
    for point in iterator_earth_fix_file():
        if point_of_country(point, country_ident):
            output += point.export_to_gpx()
    output += '</gpx>'
    return output


def main():
    with open('output.gpx', 'w') as file:
        file.truncate(0)
        file.write(points_to_gpx("LP"))


if __name__ == "__main__":
    main()
