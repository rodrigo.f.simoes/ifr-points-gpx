# IFR Points GPX

### Objective

Have the IFR points in Mapping Software

### [Downloads](https://gitlab.com/rodrigo.f.simoes/ifr-points-gpx/-/snippets)

### Importing

#### SkyDemon

![img.png](images/skydemon-sample.png)

1. Open Waypoints Menu
2. Create `New Folder`
3. Rename the created folder
4. Import `output.gpx` file
5. `Sync with Cloud`

![](images/skydemon-import.png)

##### Mobile

1. Open menu: Upper left corner cogwheel
2. `User Waypoints`
3. `Sync with Cloud`

Now on all other devices you only have to click on `Sync with Cloud` button

#### Google Earth

![](images/googleearth-example.jpg)

1. On the toolbar select `File`
2. Click button `Open...`

![](images/googleearth-openfile.png)

(Will open file explorer window)

3. Open file extension selection box
4. Select `Gps`
5. Browser for your `output.gpx` file
6. Open file

![](images/googleearth-openfileextension.png)

(Will close file explorer window and open google earth dialog)

7. Click ok

![](images/googleearth-importok.png)
